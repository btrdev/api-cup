BTR Api Cup instructies
=======================

Allereerst, zie BTR Api Basis instructies.

# Inhoudsopgave
- Initialisatie
- Gebruiker in- en uitloggen
    - Inloggen
        - Gebruikersnaam
        - Pincode
        - Onthouden/automatisch inloggen voorbereiden
    - Uitloggen
    - Automatisch inloggen
- Gebruik van ...
    - ContacturenLijst
    - Intekenen
- Structuurdiagrammen
    - Contactuur
    - Les
    - KeuzeOptie

## Initialisatie

### Cup URL
Als allereerste moet de Api Cup (hierna Api) worden ingesteld.
Dit gaat simpelweg door bijvoorbeeld:

```java
ApiCup.setCupUrl("http://www.ccgobb.cupweb6.nl");
```

Hiermee weet de Api dat hij deze url moet gebruiken om alle volgende requests op
te baseren.

Hierna kan de Api in de achtergrond gestart worden door `ApiCup.start();`.

## Gebruiker in- en uitloggen

### Inloggen
Zodra de Api opgestart is, kan er direct worden ingelogd. Indien er niet
automatisch kan worden ingelogd (zie onderste stuk) moeten er nog gegevens
worden opgevraagd van de gebruikers (zie subkopjes).

Wanneer alle gegevens verzameld zijn, is het tijd om echt in te loggen. Dit is
een simpele kwestie van de Api aanroepen met: `LogIn.logIn();`. Deze methode
geeft een LogInResultaat. Dit resultaat is, in tegenstelling tot de meeste
resultaten, een IResultaatDocument, wat betekent dat het resultaat speciaal een
document met zich meedraagt. Dit document kan worden opgevraagd door
`getDocument();` aan te roepen op het LogInResultaat.

#### Gebruikersnaam
De gebruikersnaam is een stuk tekst dat de gebruiker identificeerd. Dit is
bijvoorbeeld _Piet Pietersen (gv4a)\[12345]_. De opbouw is logisch, de naam
gevolgd door de klas tussen haken en het leerlingnummer tussen blokhaken.

Deze gebruikersnaam kan niet zo 1-2-3 worden ingevoerd, maar moet worden gezocht
door te zoeken op achternaam.
Het invoerveld dat de gebruiker te zien krijgt, moet dan ook _alleen letters_ en
een lengte van _3 tot 7 karakters_ accepteren.

Met een klik op een knop (of op de entertoets, of automatisch) kan de Api worden
aangeroepen om een zoekactie uit te voeren. Het voorbeeld hieronder geeft dit
weer hoe er naar de namen gezocht kan worden.

```java
String zoekletters = txtZoekletters.getText().trim();
ZoekNamenResultaat znr = ZoekNamen.zoek(zoekletters);

if (znr.gelukt()) {
    //Sla de zoekletters op in het globale Api-geheugen
    ApiCup.setGebruikerZoekletters(zoekletters);

    //... verdere verwerking
} else {
    //... foutafhandeling
}
```

De gebruiker waarmee de Api verdergaat, moet worden ingesteld in het globale
Api-geheugen door bijvoorbeeld: `ApiCup.setGebruiker(geselecteerdeGebruiker);`.

> __Tip:__ De zoekactie resulteert (indien gelukt) in een resultaat van
> `List<String>`. Deze List is een lijst met namen die het zoeken opleverde.
> Deze namen kunnen bijvoorbeeld in een selectielijst worden weergegeven waar de
> gebruiker weer op kan klikken om naar de volgende stap door te gaan.

#### Pincode
Als de gebruiker heeft besloten met welke persoon hij wil inloggen, moet er naar
de bijbehorende pincode gevraagd worden. Een pincode bestaat uit _exact 4
getallen_.
De pincode die de gebruiker ingeeft moet in het globale Api-geheugen worden
ingesteld door bijvoorbeeld: `ApiCup.setPincode(pincode);`.

> __Tip:__ Laat een selectievakje aan de gebruiker zien of deze wil dat zijn
> inloggegevens worden onthouden (zie volgende kopje).

#### Onthouden/automatisch inloggen voorbereiden
Met de voorgaande methodes `setGebruiker(...)` en `setPincode(...)` kan opzich
zelf een automatisch inlogsysteem geschreven worden. Maar de Api biedt hier al
ondersteuning voor aan. De gegevens worden dan namelijk opgeslagen in een
Prefmgr (zie document van BTR Api Basis).

De onderstaande code is een voor zichzelf sprekend voorbeeld.

```java
if (checkbox.isSelected()) {
    //Sla de gegevens op voor automatisch inloggen
    ApiCup.slaGegevensOp();
} else {
    //Zorg er voor dat er geen gegevens worden opgeslagen
    ApiCup.verwijderOpgeslagenGegevens();
    
    //... verdere verwerking
}
```


### Automatisch inloggen
Zodra de Api opgestart is, kan er direct bekeken worden of de Api misschien de
gebruiker automatisch kan inloggen van opgeslagen gegevens. Of dit kan, kan
worden gecontroleerd door `ApiCup.kanAutomatischInloggen();` aan te roepen.
Het beste is om dit bij het opstarten direct na te gaan. Indien er direct kan
worden ingelogd, kan dit gedaan worden door bijvoorbeeld:

```java
if (ApiCup.kanAutomatischInloggen()) {
    LogInResultaat lir = ApiCup.logAutomatischIn();
    if (lir.gelukt()) {
        nogInloggen = false;
        //... verdere verwerking
    } else {
        //... 'normale' login weergeven
    }
} else {
    //... 'normale' login weergeven
}
```

## Gebruik van ...
In de volgende gedeeltes wordt het gebruik van een paar 'deel-api's' beschreven.

### ContacturenLijst
De hoofdmethode van ContacturenLijst (CL) is de statische `contacturenLijst();` met
een jsoup-Document als argument. Dit document is dus een HTML-document dat
verkregen is bij bijvoorbeeld het inloggen (als resultaat van LogIn).
Ook na het [Intekenen](#Intekenen) wordt er een Document teruggegeven.

CL leest een document (wat dus eigenlijk de gebruiker in de browser zou laden en
daar een les kiezen) en maakt hier een logisch object van.
Dit is de opbouw van het resultaat-object:

    Map<Integer, Map<String, Contactuur>>
        k: weeknummer
        v: Map<String, Contactuur>
            k: lesuur, bijvoorbeeld do2
            v: zie structuur Contactuur

### Intekenen
De gebruiker wordt in een les ingetekend aan de hand van een KeuzeOptie, die
te vinden is in het Contactuur-object dat verkregen wordt in ContacturenLijst.
De hoofdmethode van Intekenen (IT) is de statische `intekenen();` met een
KeuzeOptie als argument.

```java
IntekenenResultaat ir = Intekenen.intekenen(geselecteerdeKeuzeOptie);

//... herlaad de view bijvoorbeeld door ContacturenLijst weer op te roepen
//    met het document uit ir.getDocument()

if (ir.gelukt()) {
    //... doe niets of geef een complimentje aan de gebruiker B)
} else {
    String foutmelding = ir.getResultaat().get("foutmelding");
    if (foutmelding != null) {
        //... laat de foutmelding zien
    }
}
```

## Structuurdiagrammen
Vrijwel alle velden uit de onderstaande structuurdiagrammen zijn via een getter
op te vragen. Zie de Javadoc voor een meer gedetailleerde beschrijving.

### Contactuur
    lesdatum Date: datum van dit contactuur
    lesuur String: zoiets als do2
    huidigeInplanning Les: zie structuur Les
    voorgepland boolean: of het een voorgeplande les is
    keuzeopties List<KeuzeOptie>: lijst met beschikbare keuzeopties, zie KeuzeOptie
    melding Melding: een eventuele Melding, zie Melding

### Les
    vak String: het vak, zoeits als biol
    lokaal String: het lokaal, zoiets als g214
    docent String: welke docent het uur geeft, zoiets als brh
    extra String: extra tekens, bijvoorbeeld toa of debat
    informatie String: extra tekst bij de les, opgegeven door docent
    getLesbeschrijving() String: gecompileerde tekst, zoals biol g035 brh
    toString() String: soort van getLesbeschrijving() maar dan voor loggen
    parseer static Map<String, Object>: zie javadoc voor meer info

### KeuzeOptie
    les Les: bijbehorende les, zie structuur Les
    isVol boolean: true als er niet meer kan worden ingetekend
    plaatsen int: aantal plaatsen (kan negatief zijn als beheerder mensen toevoegd)
    kiesnummer int: het kiesnummer dat bij Intekenen moet worden doorgegeven

### Melding
    bericht String: het bericht, kan "" zijn
    isPositief boolean: spreekt voor zich
    heeftBericht() boolean: true indien de lengte van bericht groter is dan 0