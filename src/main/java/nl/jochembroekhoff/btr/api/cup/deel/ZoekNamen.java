package nl.jochembroekhoff.btr.api.cup.deel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import nl.jochembroekhoff.btr.api.cup.ApiCup;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ZoekNamenResultaat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Zoek namen.
 *
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ZoekNamen {

    /**
     * Zoek in de namenlijst.
     *
     * @param zoekletters 3 tot 7 letters van de achternaam
     * @return een ZoekNamenResultaat met de lijst met gevonden namen
     */
    public static ZoekNamenResultaat zoek(String zoekletters) {
        List<String> namenlijst = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(ApiCup.getCupwebUrlMetId() + "/default.aspx")
                    .cookies(ApiCup.getCookies())
                    .data(ApiCup.getStandaarddata())
                    .data("_nameTextBox", zoekletters)
                    .data("_zoekButton", "Zoek")
                    .data("numberOfLettersField", "3")
                    .userAgent(ApiCup.getUA())
                    .post();

            //Selecteer de namenlijst...
            Elements elmNamenlijst = doc.select("#_nameDropDownList option");
            //...en loop alle namen langs...
            for (Element naam : elmNamenlijst) {
                //...en voeg deze stuk voor stuk toe aan het resultaat.
                namenlijst.add(naam.val());
            }

            //Zoek de standaardata uit om er voor te zorgen dat volgende
            //requests lukken.
            //Maar, voor volgende zoekverzoeken moet het wel gereset worden.
            ApiCup.zoekStandaarddataUit(doc);
        } catch (IOException ex) {
            System.out.println("[ZoekNamen]: Foutmelding: " + ex.getMessage());
            //Niet nodig, als de lengte van 'namenlijst' kleiner is dan 1, wordt
            //het resultaat automatisch op niet gelukt ingesteld.
        }

        return new ZoekNamenResultaat(namenlijst);
    }
}
