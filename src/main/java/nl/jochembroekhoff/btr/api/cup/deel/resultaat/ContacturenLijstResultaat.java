package nl.jochembroekhoff.btr.api.cup.deel.resultaat;

import nl.jochembroekhoff.btr.api.IResultaat;
import java.util.HashMap;
import java.util.Map;
import lombok.Setter;
import nl.jochembroekhoff.btr.api.cup.container.Contactuur;

/**
 * Resultaatklasse voor ContacturenLijst. Maakt gebruik van de IResultaat
 * interface.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ContacturenLijstResultaat implements IResultaat {

    @Setter
    private boolean gelukt = false;

    /**
     * ( weeknummer[13] ( lesuur[do2], contactuur[] ) )
     */
    private final Map<Integer, Map<String, Contactuur>> curen = new HashMap<>();

    public ContacturenLijstResultaat() {
        //Niets...
    }

    public void voegContactuurToe(int week, Contactuur cu) {
        if (!curen.containsKey(week)) {
            curen.put(week, new HashMap<String, Contactuur>());
        }

        curen.get(week).put(cu.getLesuur(), cu);
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

    /**
     * De lijst met contacturen.
     *
     * @return een Map met weeknummers die lesnamen met contacturen bevatten
     */
    @Override
    public Map<Integer, Map<String, Contactuur>> getResultaat() {
        return curen;
    }
}
