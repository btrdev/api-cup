package nl.jochembroekhoff.btr.api.cup.container;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Melding container.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class Melding {

    @Getter
    String bericht;

    @Getter
    boolean isPositief;

    public boolean heeftBericht() {
        return bericht.length() > 0;
    }
}
