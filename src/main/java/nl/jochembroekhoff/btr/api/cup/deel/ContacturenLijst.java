package nl.jochembroekhoff.btr.api.cup.deel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup.container.KeuzeOptie;
import nl.jochembroekhoff.btr.api.cup.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup.container.Les;
import nl.jochembroekhoff.btr.api.cup.container.Melding;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ContacturenLijstResultaat;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Stel de een lijst van Contacturen samen.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ContacturenLijst {

    public static ContacturenLijstResultaat contacturenLijst(Document doc) {
        ContacturenLijstResultaat clr = new ContacturenLijstResultaat();

        Element tabel = doc.getElementById("rooster").getElementsByTag("tbody").first();
        Elements uren = tabel.getElementsByTag("tr");

        for (Element contactuur : uren) {
            if (contactuur.hasClass("cls1") || contactuur.hasClass("cls0")) {
                Elements detabel = contactuur.getElementsByTag("td");
                int iter = 0;
                //iteraties:
                //0 = datum
                Date datum = new Date();
                //1 = uur
                String uur = "";
                //2 = 'roosterles', niet gebruikt
                //3 = gekozen les
                Les gekozenLes = Les.getLEGE_LES();
                boolean isVoorgepland = false;
                //4 = 'prekolom' voor foutmeldingen/groene bolletje
                Melding melding = null;
                //5 = keuzeopties
                List<KeuzeOptie> keuzeopties = new ArrayList<>();
                for (Element td : detabel) {
                    if (iter < 6) {
                        try {
                            switch (iter) {
                                case 2: //wordt roosterles genoemd ... niet gebruikt
                                default:
                                    break;
                                case 0: // datum
                                    datum = BtrApi.DATUM_FORMAAT.parse(td.text());
                                    break;
                                case 1: // uur
                                    uur = td.getElementsByTag("u").first().text();
                                    break;
                                case 3: // gekozen les
                                    String beschr,
                                     info = "";
                                    beschr = td.getElementsByTag("div").first().text();

                                    Elements elems = td.getElementsByTag("img");
                                    //als er minstens één link (img-tag) aanwezig is..
                                    //<img onmouseover="showHelpText('Kom eerst langs voor een afspraak!',event);" onmouseout="hideHelpText();" src="images/info2.png" border="0" width="16">
                                    if (elems.size() > 0) {
                                        //..probeer daar de informatie uit te halen
                                        info = Tools.informatieUitTagHalen(elems.first());
                                    }

                                    //gekozenLes = new Les(beschr, "lokaal", "docent", "extra", info);
                                    gekozenLes = (Les) Les.parseer(td.getElementsByTag("div").first()).get("les");

                                    //voorgeplande les
                                    isVoorgepland = td.hasClass("fixedLesson");
                                    break;
                                case 4: //'prekolom', wordt gebruikt om foutmelding/groen bolletje weer te geven
                                    if (td.html().length() > 5) { //Als er een bericht gegeven is. TODO: Getal 5? Iets anders?
                                        Element meldingImg = td.getElementsByTag("img").first();
                                        if (meldingImg != null) {
                                            //Stel de melding samen
                                            String meldingBericht = "";
                                            if (meldingImg.hasAttr("onmouseover")) { //Als er een bericht is
                                                meldingBericht = meldingImg.attr("onmouseover").replace("showHelpText('", "").replace("',event);", "");
                                                meldingBericht = BtrApi.trimHtml(meldingBericht);
                                            }
                                            boolean meldingIsPositief = true;
                                            //Als de 'src' ingesteld is op images/infoError.png betekent het een foutmelding
                                            //(Als het een positief bericht is, zou het images/greenball.gif zijn)
                                            if (meldingImg.attr("src").equals("images/infoError.png")) {
                                                meldingIsPositief = false;
                                            }

                                            //Sla de melding op
                                            melding = new Melding(meldingBericht, meldingIsPositief);
                                        }
                                    }
                                    break;
                                case 5: //keuzeopties
                                    Elements keuzeDiv = td.getElementsByTag("div");
                                    if (keuzeDiv.size() > 0) {
                                        // Loop alle keuzeopties na
                                        Elements clsButtons = keuzeDiv.first().getElementsByClass("clsButton");
                                        for (Element keuzeBtn : clsButtons) {

                                            //VERWERKING: IsVol
                                            boolean isvol = false;
                                            if (!keuzeBtn.hasAttr("onmouseover") && !keuzeBtn.hasAttr("onmouseout")) { //Als de DIV geen onmouseover en onmouseout attributen heeft, zit de les vol
                                                isvol = true;
                                            }

                                            //VERWERKING: KeuzeOptie Les & Plaatsen
                                            Map<String, Object> parseermap = Les.parseer(keuzeBtn);
                                            int plaatsen = Integer.valueOf((String) parseermap.get("plaatsen"));
                                            Les les = (Les) parseermap.get("les");

                                            //VERWERKING: Kiesnummer
                                            String onclick = keuzeBtn.attr("onclick").replace("keuzelesClicked(", "");
                                            int kommaindex = onclick.indexOf(",");
                                            int kiesnummer = Integer.parseInt(onclick.substring(0, kommaindex));

                                            //VERWERKING: Informatie
                                            //TODO
                                            //Voeg de KeuzeOptie toe
                                            keuzeopties.add(new KeuzeOptie(les, isvol, plaatsen, kiesnummer));
                                        }
                                    }
                                    break;
                            }
                        } catch (Exception e) {
                            //TODO: betere foutafhandeling en verwerking
                            e.printStackTrace();
                            System.out.println("[ContacturenLijst]: Foutmelding: " + e.getMessage());
                        }

                        iter++;
                    }
                }

                Contactuur cu = new Contactuur(datum, uur, gekozenLes, isVoorgepland, keuzeopties, melding);

                //Weeknummer berekenen
                int weekNummer = BtrApi.getWeeknummerUitDatum(datum);
                clr.voegContactuurToe(weekNummer, cu);
            }
        }

        //TODO: Een check toevoegen of alles gelukt is
        return clr;
    }
}
