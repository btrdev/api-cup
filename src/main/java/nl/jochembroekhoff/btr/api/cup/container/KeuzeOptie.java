package nl.jochembroekhoff.btr.api.cup.container;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Een keuzeoptie voor een contactuur.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class KeuzeOptie {

    /**
     * De les die deze optie beschrijft.
     */
    @Getter
    private final Les les;

    /**
     * Of de optie vol zit.
     */
    @Getter
    private final boolean isVol;

    /**
     * Aantal beschikbare plaatsen.
     */
    @Getter
    private final int plaatsen;

    /**
     * Elke mogelijke keuzeoptie heeft een uniek 'kiesnummer'. Dit wordt
     * doorgegeven bij het indienen van de optie.
     */
    @Getter
    private final int kiesnummer;
}
