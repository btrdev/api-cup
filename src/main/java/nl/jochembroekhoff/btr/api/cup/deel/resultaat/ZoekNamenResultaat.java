package nl.jochembroekhoff.btr.api.cup.deel.resultaat;

import nl.jochembroekhoff.btr.api.IResultaat;
import java.util.List;

/**
 * Resultaatklasse voor ZoekNamen. Maakt gebruik van de IResultaat interface.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ZoekNamenResultaat implements IResultaat {

    private final List<String> resultaten;
    private boolean gelukt = false;

    public ZoekNamenResultaat(List<String> resultaat) {
        resultaten = resultaat;
        if (resultaten.size() > 0) {
            gelukt = true;
        }
    }

    public void setGelukt(boolean geluktOfNiet) {
        gelukt = geluktOfNiet;
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

    @Override
    public List<String> getResultaat() {
        return resultaten;
    }
}
