package nl.jochembroekhoff.btr.api.cup.container;

import java.util.Date;
import java.util.List;
import lombok.Getter;

/**
 * Een Contactuur.
 *
 * @author Jochem Broekhoff
 */
public class Contactuur {

    @Getter
    Date lesdatum;
    @Getter
    String lesuur;
    @Getter
    Les huidigeInplanning;
    @Getter
    boolean voorgepland;
    @Getter
    List<KeuzeOptie> keuzeopties;
    @Getter
    Melding melding;

    /**
     * Maak een nieuw Contactuur.
     *
     * @param lesdatum bijv. 10-11-2016
     * @param lesuur bijv. do2
     * @param huidigeInplanning een Les
     * @param voorgepland zo ja, er kan niets worden gewijzigd (blauwe les)
     * @param keuzeOpties lijst met KeuzeOpties (indien van toepassing)
     * @param melding melding bij dit contactuur, het kan positief of negatief zijn
     */
    public Contactuur(Date lesdatum, String lesuur, Les huidigeInplanning, boolean voorgepland, List<KeuzeOptie> keuzeOpties, Melding melding) {
        this.lesdatum = lesdatum;
        this.lesuur = lesuur;
        this.huidigeInplanning = huidigeInplanning;
        this.voorgepland = voorgepland;
        this.keuzeopties = keuzeOpties;
        this.melding = melding;
    }

    /**
     * Verkrijg het lesnummer van dit contactuur. Bijvoorbeeld voor ma2 zou het
     * 2 zijn, omdat het het 2e uur op maandag is.
     *
     * @return het lesnummer
     */
    public int getLesnummer() {
        return Integer.valueOf(getLesuur().substring(2, 3));
    }
}
