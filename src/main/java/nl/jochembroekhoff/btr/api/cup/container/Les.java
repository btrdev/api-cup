package nl.jochembroekhoff.btr.api.cup.container;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.jochembroekhoff.btr.api.cup.deel.Tools;
import org.jsoup.nodes.Element;

/**
 * Een eenvoudige les.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
@AllArgsConstructor
public class Les {

    /**
     * Een standaard les
     */
    @Getter
    private static final Les LEGE_LES = new Les("ak", "g038", "abc", "", "");

    /**
     * Het vak
     */
    @Getter
    private final String vak;

    /**
     * Het lokaal
     */
    @Getter
    private final String lokaal;

    /**
     * De docent
     */
    @Getter
    private final String docent;

    /**
     * Extra gegevens
     */
    @Getter
    private final String extra;

    /**
     * Verkrijg de eventuele aanvullende informatie bij deze les. Voorbeeld:
     * Extensief luisteren deel 4.
     *
     * @return de informatie
     */
    @Getter
    private final String informatie;

    /**
     * Verkrijg de lesbeschrijving. Voorbeeld: ckv g142 nwf
     *
     * @return de lesbeschrijving
     */
    public final String getLesbeschrijving() {

        return vak + " " + lokaal + " " + docent + " " + extra;
    }
    
    @Override
    public final String toString() {
        return "Les{" + getLesbeschrijving() + "}";
    }

    /**
     * Parseer een String naar meerdere objecten. Zie 'returns'.
     *
     * @param parseerElement invoer die mogelijk plaatsinformatie bevat
     * @return een Map met als keys: les, plaatsen en als values een Les en een
     * Integer met de betreffende waarden
     */
    public static final Map<String, Object> parseer(Element parseerElement) {
        Map<String, Object> map = new HashMap<>();

        String invoer = parseerElement.text();

        String[] split = invoer.split(" ");
        int teller = 1;
        String vak = "", lokaal = "", docent = "", extra = "", informatie = "";
        //Haal de informatie er uit zo mogelijk
        if (parseerElement.getElementsByTag("img").size() > 0) {
            informatie = Tools.informatieUitTagHalen(parseerElement.getElementsByTag("img").first());
        }
        for (String tekst : split) {
            if (tekst.startsWith("[") && tekst.endsWith("]")) {
                //VERWERKING: Plaatsen
                tekst = tekst.replace("[", "").replace("]", "");
                map.put("plaatsen", tekst);
            } else if (tekst.length() > 0) {
                //VERWERKING: KeuzeOptie Les
                switch (teller) {
                    //Voor TOA en DEB gelden iets afwijkende regels
                    case 1:
                        vak = tekst;
                        break;
                    case 2:
                        switch (tekst) {
                            case "toa":
                            case "deb":
                                extra = tekst;
                                break;
                            default:
                                lokaal = tekst;
                                break;
                        }
                        break;
                    case 3:
                        switch (extra) {
                            case "toa":
                            case "deb":
                                lokaal = tekst;
                                break;
                            default:
                                docent = tekst;
                                break;
                        }
                        break;
                    case 4:
                        switch (extra) {
                            case "toa":
                            case "deb":
                                docent = tekst;
                                break;
                            default:
                                break;
                        }
                        break;
                }
            }

            teller++;
        }

        map.put("les", new Les(vak, lokaal, docent, extra, informatie));

        return map;
    }

}
