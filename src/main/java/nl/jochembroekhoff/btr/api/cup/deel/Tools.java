package nl.jochembroekhoff.btr.api.cup.deel;

import org.jsoup.nodes.Element;

/**
 * BtrCupApi Tools.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class Tools {

    public static String informatieUitTagHalen(Element link) {
        String onmouseover = link.attr("onmouseover");
        String terug = "";
        if (onmouseover.length() > 0) {
            terug = onmouseover.replace("showHelpText('", "")
                    .replace("',event);", "")
                    .trim();
        }
        //Newlines in de tabel?
        //MANIER 1: (werkt niet)
        //terug = terug.replace("<br />", "\n");
        //terug = terug.replace("<br/>", "\n");
        //terug = terug.replace("<br>", "\n");
        //MANIER 2: (werkt, maar dan krijg je een rare tabel...)
        //terug = "<html>" + terug + "</html>";
        System.out.println("[Tools/InformatieUitTagHalen]: Gevonden: " + terug);
        return terug;
    }
}
