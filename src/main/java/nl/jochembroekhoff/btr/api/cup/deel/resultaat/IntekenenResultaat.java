package nl.jochembroekhoff.btr.api.cup.deel.resultaat;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.jsoup.nodes.Document;
import nl.jochembroekhoff.btr.api.IResultaatDocument;

/**
 * Resultaatklasse voor Intekenen. Maakt gebruik van de IResultaat interface.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class IntekenenResultaat implements IResultaatDocument {

    @Getter
    @Setter
    private Document document;

    @Setter
    private boolean gelukt = false;
    private final Map<String, String> resultaatSet = new HashMap<>();

    public IntekenenResultaat() {
        //Niets...
    }

    /**
     * Als het gelukt is, kan het document in getDocument() gevonden worden. Als
     * het niet gelukt is, is er waarschijnlijk een foutmelding te vinden (als
     * String) in het resultaat-object van getResultaat() met als key
     * "foutmelding".
     *
     * @return of deze Api-call gelukt is (en dus geen foutmelding heeft)
     */
    @Override
    public boolean gelukt() {
        return gelukt;
    }

    /**
     * Stel de foutmelding in. Let op: het resultaat wordt direct als
     * niet-gelukt ingesteld.
     *
     * @param foutmelding de foutmelding, mag regeleinden bevatten
     */
    public void setFoutmelding(String foutmelding) {
        resultaatSet.put("foutmelding", foutmelding);
        gelukt = false;
    }

    /**
     * De lijst met contacturen. Indien er een fout is opgetreden, wordt deze
     * gezet in de resultaatmap met de key "foutmelding".
     *
     * @return een Map met weeknummers die lesnamen met contacturen bevatten
     */
    @Override
    public Map<String, String> getResultaat() {
        return resultaatSet;
    }
}
