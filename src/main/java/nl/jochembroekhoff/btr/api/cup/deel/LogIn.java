package nl.jochembroekhoff.btr.api.cup.deel;

import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import nl.jochembroekhoff.btr.api.cup.ApiCup;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.LogInResultaat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * BtrCupApi Login.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class LogIn {

    /**
     * Log in met de ingestelde gegevens. Deze gegevens moeten worden ingesteld
 in ApiCup.
     *
     * @return het LogInResultaat met een document
     */
    public static LogInResultaat logIn() {
        
        LogInResultaat lir = new LogInResultaat(false);
        
        try {
            Document doc = Jsoup.connect(ApiCup.getCupwebUrlMetId() + "/LogInWebForm.aspx")
                    .cookies(ApiCup.getCookies())
                    .data(ApiCup.getStandaarddata())
                    .data("_nameDropDownList", ApiCup.getGebruiker())
                    .data("_pincodeTextBox", ApiCup.getPincode())
                    .data("_loginButton", "Login")
                    .userAgent(ApiCup.getUA())
                    .post();
            
            boolean allesGelukt = true;
            String foutTekst;
            if (doc.getElementById("_errorLabel") != null) {
                Element errorLabel = doc.getElementById("_errorLabel");
                if (errorLabel.text().trim().equals("Error Label")) {
                    foutTekst = "";
                } else {
                    allesGelukt = false;
                    foutTekst = errorLabel.text();
                }
            } else {
                foutTekst = "";
            }
            
            if (allesGelukt) {
                //Instellen dat het gelukt is
                lir.setGelukt(true);
                lir.setDocument(doc);
                
                System.out.println("[LogIn Action]: LOGIN GRANTED");
            } else {
                System.out.println("[LogIn Action]: LOGIN FAILED");
                lir.setFoutmelding(foutTekst);
            }

            //Zoek de standaardata uit om er voor te zorgen dat volgende
            //requests lukken.
            //Maar, voor volgende zoekverzoeken moet het wel gereset worden.
            ApiCup.zoekStandaarddataUit(doc);
        } catch (IOException ex) {
            System.out.println("[LogIn]: Foutmelding: " + ex.getMessage());
            //Niet nodig, als de lengte van 'namenlijst' kleiner is dan 1, wordt
            //het resultaat automatisch op niet gelukt ingesteld.
        }
        
        return lir;
    }
}
