package nl.jochembroekhoff.btr.api.cup.deel.resultaat;

import nl.jochembroekhoff.btr.api.IResultaatDocument;
import lombok.Setter;
import org.jsoup.nodes.Document;

/**
 * Resultaatklasse voor LogIn.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class LogInResultaat implements IResultaatDocument {

    private boolean gelukt = false;
    private Document doc;
    @Setter
    private String foutmelding = "";

    public LogInResultaat(boolean gelukt) {
        this.gelukt = gelukt;
    }

    public void setGelukt(boolean geluktOfNiet) {
        gelukt = geluktOfNiet;
    }

    /**
     * Kijk of deze loginpoging gelukt is. Als het niet zo is, is er
     * waarschijnlijk een foutmelding aanwezig in getResultaat().
     *
     * @return of deze loginpoging gelukt is
     */
    @Override
    public boolean gelukt() {
        return gelukt;
    }

    /**
     * Verkrijg het resultaat. Dit is een string met de eventuele foutmelding.
     *
     * @return de foutmelding
     */
    @Override
    public String getResultaat() {
        return "";
    }

    @Override
    public void setDocument(Document doc) {
        this.doc = doc;
    }

    @Override
    public Document getDocument() {
        return this.doc;
    }
}
