package nl.jochembroekhoff.btr.api.cup.deel;

import java.io.IOException;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import nl.jochembroekhoff.btr.api.cup.ApiCup;
import nl.jochembroekhoff.btr.api.cup.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup.container.KeuzeOptie;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ContacturenLijstResultaat;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.IntekenenResultaat;

/**
 * Intekenen voor een KeuzeOptie.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class Intekenen {

    /**
     * Intekenen voor een KeuzeOptie.
     *
     * @param keuzeoptie KeuzeOptie object
     * @return het intekenresultaat
     */
    public static IntekenenResultaat intekenen(KeuzeOptie keuzeoptie) {
        IntekenenResultaat ir = new IntekenenResultaat();
        ir.setGelukt(true); //Standaard op Gelukt zetten

        int kiesnummer = keuzeoptie.getKiesnummer();
        
        try {
            Document doc = Jsoup.connect(ApiCup.getCupwebUrlMetId() + "/RoosterForm.aspx")
                    .cookies(ApiCup.getCookies())
                    .data(ApiCup.getStandaarddata())
                    .data("_nameDropDownList", ApiCup.getGebruiker())
                    .data("_pincodeTextBox", ApiCup.getPincode())
                    //Specifieke Inteken-data
                    .data("__SCROLLPOSITIONX", "0")
                    .data("__SCROLLPOSITIONY", "0")
                    .data("__EVENTARGUMENT", kiesnummer + "")
                    .userAgent(ApiCup.getUA())
                    .post();
            
            //Stel het document in in het resultaat
            ir.setDocument(doc);

            //Vraag een nieuwe ContacturenLijst op. TODO: Check of isGelukt()
            ContacturenLijstResultaat clr = ContacturenLijst.contacturenLijst(doc);
            Map<Integer, Map<String, Contactuur>> curen = clr.getResultaat();
            
            for (Map<String, Contactuur> curenDezeweek : curen.values()) {
                for (Contactuur cu : curenDezeweek.values()) {
                    //Contactuur cu = cuur.getValue();
                    if (cu.getMelding() != null) {
                        String bericht = cu.getMelding().getBericht();
                        if (!cu.getMelding().isPositief()) {
                            ir.setGelukt(false);
                            ir.setFoutmelding(bericht);
                        }
                    }
                }
            }

            //Zoek de standaardata uit om er voor te zorgen dat volgende
            //requests lukken.
            ApiCup.zoekStandaarddataUit(doc);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            //TODO: Foutafhandeling
        }
        
        return ir;
    }
}
