package nl.jochembroekhoff.btr.api.cup;

import nl.jochembroekhoff.btr.api.cup.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup.deel.LogIn;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static nl.jochembroekhoff.btr.api.BtrApi.getPrefContainer;
import nl.jochembroekhoff.btr.api.IBtrApi;
import nl.jochembroekhoff.btr.api.cup.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ZoekNamenResultaat;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Cupweb API basisklasse, voor instellingen etc. API rev 1 voor CUP 4.0.9.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ApiCup implements IBtrApi {

    //Instellingen
    private static String CUP_URL = "";
    private static String GLOBALE_ID = "(S(drggm3gv0yvzavcem5xiv1r5))"; //Voorbeeld!
    private static final String PREFS_GEBRUIKERSGEGEVENS = "desktop-cup-gebruikersgegevens";

    //'minicache'
    private static String GEBRUIKER = "";
    private static String GEBRUIKER_ZOEKLETTERS = "";
    private static String PINCODE = "";

    //Automatisch inloggen
    private static boolean kanautomatischinloggen = false;

    //Standaarddata
    private static final String[] STDLIJST = {"__EVENTTARGET", /*"__EVENTARGUMENT",*/ "__VIEWSTATE", "__VIEWSTATEGENERATOR", "__EVENTVALIDATION"};
    private static final Map<String, String> STANDAARDDATA = new HashMap<>();
    private static final Map<String, String> COOKIES = new HashMap<>();
    private static final String VERSIE = "1.0.0";

    public static void start() {
        //Let op: onderstaande data is voorbeelddata, waarschijnlijk niet geldig
        //DriverBase.STANDAARDDATA.put("__EVENTARGUMENT", "");
        ApiCup.STANDAARDDATA.put("__EVENTTARGET", "");
        ApiCup.STANDAARDDATA.put("__EVENTVALIDATION", "/wEWBALBlcH0DgK52+LYCQK1gpH7BAL0w/PHASdiVb7NKucbK9WECIbeQiTOkb/fhXFleYw0NjqKgwkQ"); //Voorbeeld!
        ApiCup.STANDAARDDATA.put("__VIEWSTATE", "/wEPDwULLTE3NDM5MzMwMzRkZEjOqOKmjjO7x1bTriNE46l0BGvTdnW+v/x5RBZ1JlN5"); //Voorbeeld!
        ApiCup.STANDAARDDATA.put("__VIEWSTATEGENERATOR", "CA0B0334"); //Homepagina (voorbeeld!)

        ApiCup.COOKIES.put("ASP.NET_SessionId", "");

        try {
            //COOKIES EN GLOBALE_ID LADEN
            Connection.Response cookiesKrijgen = Jsoup.connect(ApiCup.getCupwebUrl() + "/default.aspx")
                    .method(Connection.Method.GET)
                    .execute();

            //GLOBALE_ID
            String pad = cookiesKrijgen.url().getPath(); //Iets als /(S(drggm3gv0yvzavcem5xiv1r5))/default.aspx
            pad = pad.replace("/default.aspx", ""); // /default.aspx weghalen
            pad = pad.replace("/", ""); // De / aan het begin weghalen
            ApiCup.GLOBALE_ID = pad;

            //Alle cookies instellen
            ApiCup.COOKIES.putAll(cookiesKrijgen.cookies());

            //Probeer voor de eerste keer Standaarddata uit te zoeken
            ApiCup.zoekStandaarddataUit(cookiesKrijgen.parse());
        } catch (IOException ex) {
            //Niets
            System.out.println("[ApiBasis/Start]: Foutmelding: " + ex.getMessage());
        }

        String gebr = getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).get("gebruikersnaam", "-");
        String zoekl = getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).get("zoekletters", "-");
        int pinc = getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).getInt("pincode", 0);
        if (!gebr.equals("-")
                && !zoekl.equals("-")
                && pinc > 0) {
            ApiCup.kanautomatischinloggen = true;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //INSTELLINGEN:
    public static void setCupUrl(String url) {
        ApiCup.CUP_URL = url;
    }

    public static String getCupwebUrl() {
        return ApiCup.CUP_URL;
    }

    public static String getCupwebUrlMetId() {
        return ApiCup.CUP_URL + "/" + ApiCup.GLOBALE_ID;
    }

    /**
     * UserAgent.
     *
     * @return de UA string
     */
    public static String getUA() {
        return "BtrApiCup/" + ApiCup.getVersie() + "/" + ApiCup.GLOBALE_ID;
    }

    ////////////////////////////////////////////////////////////////////////////
    //GEBRUIKERSDATA:

    public static void setGebruiker(String gebruiker) {
        ApiCup.GEBRUIKER = gebruiker;
    }

    public static String getGebruiker() {
        return ApiCup.GEBRUIKER;
    }

    public static void setGebruikerZoekletters(String zoekletters) {
        ApiCup.GEBRUIKER_ZOEKLETTERS = zoekletters;
    }

    public static void setPincode(String pincode) {
        ApiCup.PINCODE = pincode;
    }

    public static String getPincode() {
        return ApiCup.PINCODE;
    }

    /**
     * Sla de inloggegevens die op dit moment aanwezig zijn op.
     */
    public static void slaGegevensOp() {
        getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).set("gebruikersnaam", GEBRUIKER);
        getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).set("zoekletters", GEBRUIKER_ZOEKLETTERS);
        getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).setInt("pincode", Integer.parseInt(PINCODE));
    }

    /**
     * Verwijder simpelweg alle gegevens uit de GEBRUIKERSGEGEVENS node.
     */
    public static void verwijderOpgeslagenGegevens() {
        getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).clear();
    }

    ////////////////////////////////////////////////////////////////////////////
    //AUTOMATISCH INLOGGEN:
    public static boolean kanAutomatischInloggen() {
        return ApiCup.kanautomatischinloggen;
    }

    /**
     * Stel de gegevens in vanuit de PREFS.
     */
    private static void laadAutologinGegevens() {
        String gebr = getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).get("gebruikersnaam", "-");
        String zoekl = getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).get("zoekletters", "-");
        String pinc = getPrefContainer(PREFS_GEBRUIKERSGEGEVENS).get("pincode", "1234");

        ApiCup.GEBRUIKER = gebr;
        ApiCup.GEBRUIKER_ZOEKLETTERS = zoekl;
        ApiCup.PINCODE = pinc;
    }

    /**
     * Log automatisch in.
     *
     * @return het LogInResultaat met de logindata
     */
    public static LogInResultaat logAutomatischIn() {
        LogInResultaat lir = new LogInResultaat(false);

        if (ApiCup.kanautomatischinloggen) {
            ApiCup.laadAutologinGegevens();

            //Zoek eerst, zonder dat slikt CUP het niet
            ZoekNamenResultaat znr = ZoekNamen.zoek(ApiCup.GEBRUIKER_ZOEKLETTERS);
            if (znr.gelukt()) {
                lir = LogIn.logIn();
            }
        }

        return lir;
    }

    ////////////////////////////////////////////////////////////////////////////
    //STANDAARDDATA:
    public static Map<String, String> getStandaarddata() {
        return ApiCup.STANDAARDDATA;
    }

    public static Map<String, String> getCookies() {
        return ApiCup.COOKIES;
    }

    /**
     * (Wordt automatisch aangeroepen) Zoek de standaarddata uit van een
     * teruggekregen document van een api-call.
     *
     * @param doc het document
     */
    public static void zoekStandaarddataUit(Document doc) {
        for (String std : ApiCup.STDLIJST) {
            try {
                Element stdElem = doc.getElementById(std);
                if (stdElem != null) {
                    if (stdElem.val() != null) {
                        ApiCup.STANDAARDDATA.put(std, doc.getElementById(std).val());
                    }
                }
            } catch (Exception e) {
                System.out.println("[ApiBasis/Standaarddata]: Foutmelding: " + e.getMessage());
                e.printStackTrace();
            }
        }

        //Vervang alle null-waarden door ""
        for (Map.Entry<String, String> entry : ApiCup.STANDAARDDATA.entrySet()) {
            if (entry.getValue() == null) {
                ApiCup.STANDAARDDATA.put(entry.getKey(), "");
            }
        }
    }

    public static String getVersie() {
        return ApiCup.VERSIE;
    }

    ////////////////////////////////////////////////////////////////////////////
    //TOOLS:

    /**
     * Verkrijg de naam van een lesuur op basis van het gegeven contactuur.
     * Bijvoorbeeld: Maandag 2e.
     *
     * @param cu het contactuur
     * @return naam van het lesuur
     */
    public static String getLesuurNaam(Contactuur cu) {
        int lesnummerUur = cu.getLesnummer();
        switch (cu.getLesdatum().getDay()) {
            case 1:
                return "Maandag " + lesnummerUur + "e";
            case 2:
                return "Dinsdag " + lesnummerUur + "e";
            case 3:
                return "Woensdag " + lesnummerUur + "e";
            case 4:
                return "Donderdag " + lesnummerUur + "e";
            case 5:
                return "Vrijdag " + lesnummerUur + "e";
        }
        return cu.getLesuur();
    }
}
